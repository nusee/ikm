%% IKM homework main module

%% if we have a saved classifier, we load it
if exist('classifier.mat', 'file')
    load classifier;
   
%% otherwise create everything from scratch
else
    
    %% create database of images
    % read images from 'img'
    % create 'db' with folders for each classifier
    % create 150x150px images (should be sufficient)
    % search for 500x500px face size (based on our test images)
    createImgDb('img', 'db', [150 150], [500 500]);
    
    %% detect features of all images in 'db' folder
    [HOG, labels] = detectFeatures('db');
    
    %% create and save face classifier
    faceClassifier = fitcecoc(HOG, labels);    
    save('classifier.mat', 'faceClassifier');
    
end

%% Run webcam classifier.
classifyWebcam(faceClassifier, [640 480]);

