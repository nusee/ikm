%% Recognize faces on live webcam input
function classifyWebcam(model, resolution)

%% Create face detector
FDetect = vision.CascadeObjectDetector('FrontalFaceCART', 'MinSize', [30 30]);

%% Create and setup webcam
cam = webcam;
cam.resolution = strcat(num2str(resolution(1)), 'x', num2str(resolution(2)));

%% Create the video player object
videoPlayer = vision.VideoPlayer('Position', [100 100 resolution(1) resolution(2)]);

%% Main loop, run until the video window is closed.
runLoop = true;
while runLoop
    
    %% Take a snapshot from webcam, convert to grayscale and detect faces.
    img = snapshot(cam);
    gs = rgb2gray(img);
    BB = step(FDetect, gs);
    
    %% Run each found face through the classifier, and label the image.
    for i=1:size(BB)
        face = imcrop(gs, BB(i,:));
        face = imresize(face, [150 150]);
        features = extractHOGFeatures(face);
        [label, score] = predict(model, features);
        if abs(max(score)) < 0.2
            img = insertObjectAnnotation(img, 'rectangle', BB(i,:), label);
        end
       
    end
    
    %% Output video frame.
    step(videoPlayer, img);
    runLoop = isOpen(videoPlayer);
end

release(videoPlayer);
release(FDetect);
end
