%% Detects and extracts HOG features from images in the database folder
% String dbFolder:  Path to the image database folder.
%                   This folder must contain subfolders, where the *.jpg
%                   reference images are stored.%
% labels:           Label for the reference image class, one column per
%                   image. The nth row in HOGFeatures is in the class of
%                   the nth column of this array. The class name is the
%                   same as the folder name the image is stored in.
% HOGFeatures:      The extracted feature descriptors using Histograms of
%                   Oriented Gradients. It has as many rows as the number
%                   of the reference pictures.

function [HOGFeatures, labels] = detectFeatures(dbFolder)


%% read the input image directory
directory = dir(dbFolder);
subfolders = [directory(:).isdir];
nameSubfolders = {directory(subfolders).name};
nameSubfolders(ismember(nameSubfolders,{'.','..'})) = [];

folderNum = size(nameSubfolders, 2);


%% iterate through directories
iter = 0;
for folder = 1:folderNum
    folderName = nameSubfolders(1,folder);
    folderName = folderName{1,1};
    currentImagePath = strcat(dbFolder, '\', folderName, '\*.jpg');
    images = dir(currentImagePath);
    
    imgNum = size(images,1);
    
    for image = 1:imgNum
        iter = iter + 1;
        
         %% read current img to process
        currentImgName = strcat(dbFolder, '\', folderName, '\', images(image).name);
        img = imread(currentImgName);

        %% get HOG features
        [features] = extractHOGFeatures(img, 'CellSize', [32 32]);
        HOGFeatures(iter,:) = features;
        labels(iter) = {folderName};
    end

end

end
