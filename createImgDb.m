
%% Detect faces on images using Viola-Jones Algorithm
% export dim x dim sized grayscale images of detected faces
%
%
% String imgPath : path of images (relative)
%
% root
% |- imgPath
%    |- person 1
%    |- person 2
%    |- ...
%
%
% String dbPath : path to stored images folder (relative)
%
% [int int] imageDimensions : created images' dimensions in pixels
%
% [int int] minSize : minimum size for face match


function createImgDb(imgPath, dbPath, imgDimensions, minSize)

%% create db folder if not exists
if exist(dbPath, 'dir') ~= 7
    mkdir(dbPath);
end

%% handler to detect faces
FDetect = vision.CascadeObjectDetector('FrontalFaceCART', 'MinSize', minSize);

%% read the input image directory
directory = dir(imgPath);
subfolders = [directory(:).isdir];
nameSubfolders = {directory(subfolders).name};
nameSubfolders(ismember(nameSubfolders,{'.','..'})) = [];

%% iterate through directories
for folder = 1:size(nameSubfolders, 2)
    currentImagePath = strcat(imgPath, '\', nameSubfolders(folder), '\*.jpg');
    images = dir(currentImagePath{1,1});
    
    %% create correspondent subfolder
    if exist(strcat(dbPath, '\', nameSubfolders{1,folder}), 'dir') ~= 7
        mkdir(dbPath, nameSubfolders{1,folder})
    end
    
    %% iterate through images
    for image = 1:size(images)
        
        %% read current img to process
        currentImgName = strcat(imgPath, '\', nameSubfolders{1,folder}, '\', images(image).name);
        img = imread(currentImgName);
        
        %% returns Bounding Box values based on number of objects
        BB = step(FDetect, img);
        
        %% crop face and convert it to gray
        if ~isempty(BB)
            
            % crop
            cropped = imcrop(img, BB(1,:));
            
            % resize
            resized = rgb2gray(imresize(cropped, imgDimensions));
            
            % concat the file name and make the image
            filename = strcat(dbPath, '\', nameSubfolders(1,folder), '\', num2str(image), '.jpg');
            imwrite(resized, filename{1,1});
            
        end % faces iteration
        
    end % images iteration
    
end % directory iteration

%% return: void
disp('Image database created.');

end % end function